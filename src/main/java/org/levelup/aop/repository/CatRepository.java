package org.levelup.aop.repository;

import org.levelup.aop.domain.CatEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CatRepository extends CrudRepository<CatEntity, Integer> {

    Optional<CatEntity> findByName(String name);

}
