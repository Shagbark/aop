package org.levelup.aop.configuration;

import com.zaxxer.hikari.HikariDataSource;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@EnableTransactionManagement
@Configuration
@PropertySource("classpath:database.properties")
public class JpaConfiguration {

    @Value("${chat.db.host}")
    private String host;
    @Value("${chat.db.name}")
    private String dbName;
    @Value("${chat.db.port}")
    private String port;
    @Value("${chat.db.username}")
    private String username;
    @Value("${chat.db.password}")
    private String password;
    @Value("${chat.db.version}")
    private String version;

    @Bean
    public DataSource dataSource() {
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        dataSource.setJdbcUrl(String.format("jdbc:postgresql://%s:%s/%s", host, port, dbName));
        return dataSource;
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

}
