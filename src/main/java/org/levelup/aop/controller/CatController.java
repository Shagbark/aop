package org.levelup.aop.controller;

import org.levelup.aop.domain.Cat;
import org.levelup.aop.service.CatService;
import org.levelup.aop.service.CatServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cat")
public class CatController {

    private final CatService catService;

    @Autowired
    public CatController(CatService catService) {
        this.catService = catService;
    }

    @GetMapping
    public Cat findByName(@RequestParam("name") final String name) {
        catService.findById(null);
        catService.doSmth();
        return catService.findByName(name);
    }

}
