package org.levelup.aop.service;

import org.levelup.aop.domain.Cat;

public interface CatService {

    Cat findByName(String name);

    Cat findById(Integer id);

    void doSmth();

}
