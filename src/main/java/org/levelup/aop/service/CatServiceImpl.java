package org.levelup.aop.service;

import org.levelup.aop.aspect.Logging;
import org.levelup.aop.aspect.Profiling;
import org.levelup.aop.domain.Cat;
import org.levelup.aop.repository.CatRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class CatServiceImpl implements CatService {

    private final CatRepository catRepository;
    private final ModelMapper modelMapper;

    private Map<String, Cat> cats;

    @Autowired
    public CatServiceImpl(CatRepository catRepository, ModelMapper modelMapper) {
        this.catRepository = catRepository;
        this.modelMapper = modelMapper;
        this.cats = new HashMap<>();
    }

    //@Override
    @Profiling
    @Logging
    public Cat findByName(String name) {
        System.out.println("Method");
        // Verify user
        // Count statistics
        // ..

        Cat cachedCat = cats.get(name);
        if (cachedCat != null) {
            return cachedCat;
        }

        return catRepository.findByName(name)
                .map(entity -> {
                    Cat cat = modelMapper.map(entity, Cat.class);
                    cats.put(cat.getName(), cat);
                    return cat;
                })
                .orElse(null);
    }


    //@Override
    public Cat findById(Integer id) {
        return null;
    }

    //@Override
    public void doSmth() {

    }
}
