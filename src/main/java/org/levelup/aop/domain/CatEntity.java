package org.levelup.aop.domain;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "cat")
@Setter
@Getter
@NoArgsConstructor
public class CatEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String breed;

}
