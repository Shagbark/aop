package org.levelup.aop.domain;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
public class Cat {

    private Integer id;
    private String name;
    private String breed;

}
