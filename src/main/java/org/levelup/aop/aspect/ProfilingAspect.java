package org.levelup.aop.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Configurable
// factory-method="aspectOf"
public class ProfilingAspect {

    @Pointcut("@annotation(org.levelup.aop.aspect.Profiling)")
    public void profilingMethod() {}

    @Pointcut("execution(org.levelup.aop.domain.Cat org.levelup.aop.service.CatServiceImpl.*(..))")
    public void loggingMethod() {}

    // 1 - pointcut ( &&, ||)
    // 2 - @annotation(org.levelup.aop.aspect.Profiling)
    @Around("loggingMethod()")
    // Advice
    public Object process(ProceedingJoinPoint pjp) throws Throwable {
        long millis = System.currentTimeMillis();
        Object result = pjp.proceed();
        System.out.println("Execution time: " + (System.currentTimeMillis() - millis));
        return result;
    }

//    @Around("loggingMethod() && profilingMethod()")
//    // Advice
//    public Object process2(ProceedingJoinPoint pjp) throws Throwable {
//        long millis = System.currentTimeMillis();
//        Object result = pjp.proceed();
//        System.out.println("Execution time2 : " + (System.currentTimeMillis() - millis));
//        return result;
//    }

}
